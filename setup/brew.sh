#!/bin/bash

# Check for Homebrew, install if we don't have it
if test ! $(which brew); then
    echo "Installing homebrew..."
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Make sure we’re using the latest Homebrew.
brew update

# Upgrade any already-installed formulae.
brew upgrade

# Install useful binaries
PACKAGES=(
    git
    node
    yarn
    tmux
    tree
    bat
    ssh-copy-id
    koekeishiya/formulae/skhd
)
echo "Installing packages..."
brew install ${PACKAGES[@]}

# Install casks
CASKS=(
    google-chrome
    firefox
    webtorrent
    hyper
    alfred
    visual-studio-code
    fontbase
    mpv
    # cscreen dead?
)
echo "Installing cask apps..."
brew cask install ${CASKS[@]}

# Install fonts
brew tap caskroom/fonts
FONTS=(
    font-inconsolidata
    font-roboto
    font-fira-code
    font-hack-nerd-font
)
echo "Installing fonts..."
brew cask install ${FONTS[@]}

echo "Cleaning up..."
brew cleanup
