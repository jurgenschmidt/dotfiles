#!/bin/bash

pushd /tmp/

curl -s https://api.github.com/repos/Peltoche/lsd/releases/latest \
| grep "browser_download_url.*apple-darwin" \
| cut -d ":" -f 2,3 \
| tr -d \" \
| wget -qi -

tarball="$(find . -name "*apple-darwin.tar.gz" 2>/dev/null)"
tar -xzf $tarball

folder="$(find . -type d -name "*apple-darwin")"

chmod +x $folder/lsd

sudo mv $folder/lsd /usr/local/bin/
