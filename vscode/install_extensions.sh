#!/bin/bash

EXTENSIONS_FILE="./extensions-list.txt"

extensions="$(cat "$EXTENSIONS_FILE")"

echo "Installing vscode extensions..."

for extension in $extensions; do
  code --install-extension "${extension}"
done

echo "[!] vscode extensions done"
