#!/bin/bash

EXTENSIONS_FILE="./extensions-list.txt"

code --list-extensions | sort > "$EXTENSIONS_FILE"
