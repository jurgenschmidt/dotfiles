# ~/dotfiles
First time run is `./bootstrap.sh` & `./macos.sh`.

## VSCode
### Plugins workaround
- Make the list of extensions running the `./vscode/make_extensions_list.sh`
- Install the extensions with `./vscode/install_extensions.sh`
