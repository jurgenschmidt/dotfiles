export ZSH=$HOME/.oh-my-zsh
export LC_ALL=en_US.UTF-8
export PATH=$HOME:$PATH:/usr/local/sbin
export NVM_DIR="$HOME/.nvm"

ZSH_THEME="stupidfix"
COMPLETION_WAITING_DOTS="true"

plugins=(git common-aliases zsh-autosuggestions zsh-syntax-highlighting)

# Use Java 8
export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)

# Add Visual Studio Code `code`
export PATH=$PATH:/Applications/Visual\ Studio\ Code.app/Contents/Resources/app/bin
# Yarn
export PATH=$PATH:/usr/local/Cellar/node/8.1.1/bin
# NVM
source $(brew --prefix nvm)/nvm.sh

source $ZSH/oh-my-zsh.sh

# Aliases
alias c='clear'
alias gs='git status'
alias tree='tree -F --dirsfirst -C'
alias cat='bat'
alias ping='prettyping --nolegend'
alias lsd='lsd -la'

# tabtab source for electron-forge package
# uninstall by removing these lines or running `tabtab uninstall electron-forge`
[[ -f /Users/jurgen/Projects/new-dashboard/node_modules/tabtab/.completions/electron-forge.zsh ]] && . /Users/jurgen/Projects/new-dashboard/node_modules/tabtab/.completions/electron-forge.zsh
