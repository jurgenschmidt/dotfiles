#!/bin/bash

# Ask for the administrator password upfront
sudo -v
# Keep-alive: update existing sudo time stamp until the script has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# Install homebrew
./setup/brew.sh

# Install oh-my-zsh
./setup/zsh.sh

# Install lsd (https://github.com/Peltoche/lsd)
./setup/lsd.sh

# Install vscode extensions
./vscode/install_extensions.sh

# Install prettyping

## FONTS
brew tap caskroom/fonts
brew cask install font-fira-code

# Git global configs
ln -sf ~/dotfiles/git/gitconfig ~/.gitconfig
ln -sf ~/dotfiles/git/gitignore ~/.gitignore

# Zsh config
ln -sf ~/dotfiles/zsh/.zshrc ~/.zshrc

# mpv configs
ln -sf ~/dotfiles/mpv/mpv.conf ~/.config/mpv/mpv.conf
ln -sF ~/dotfiles/mpv/scripts ~/.config/mpv

# vscode configs
ln -sf ~/dotfiles/VSCode/settings.json ~/Library/Application\ Support/Code/User/settings.json
ln -sf ~/dotfiles/VSCode/keybindings.json ~/Library/Application\ Support/Code/User/keybindings.json
ln -sF ~/dotfiles/VSCode/snippets/ ~/Library/Application\ Support/Code/User/

# hyper config
ln -sf ~/dotfiles/hyper/.hyper.js ~/.hyper.js

# skhd config
ln -sf ~/dotfiles/skhd/.skhdrc ~/.skhdrc
